class Post extends Backbone.Model
	urlRoot: 'http://jsonplaceholder.typicode.com/posts'
	defaults: ->
		userId: null
		title: null
		body: null

class PostList extends Backbone.Collection
	url: 'http://jsonplaceholder.typicode.com/posts'
	model: Post

######### VIEW MODELS	

class PostViewModel extends kb.ViewModel
	constructor: (post) ->
		@title = kb.observable(post, 'title')
		@body = kb.observable(post, 'body')

class PostListViewModel extends kb.ViewModel
	constructor: (posts) ->
		@posts = kb.collectionObservable(posts, {view_model: PostViewModel})


########## MAIN

$ ->
	(window.posts = new PostList()).fetch()

	ko.applyBindings(new PostListViewModel(posts), $('#list')[0])





